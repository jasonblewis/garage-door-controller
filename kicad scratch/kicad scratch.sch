EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L jbl~kikad~library:2N7002DW Q1
U 1 1 5EBC6395
P 3750 3450
F 0 "Q1" H 3750 3937 60  0000 C CNN
F 1 "2N7002DW" H 3750 3831 60  0000 C CNN
F 2 "digikey-footprints:SOT-363" H 3950 3650 60  0001 L CNN
F 3 "https://www.onsemi.com/pub/Collateral/2N7002DW-D.PDF" H 3950 3750 60  0001 L CNN
F 4 "2N7002DWCT-ND" H 3950 3850 60  0001 L CNN "Digi-Key_PN"
F 5 "2N7002DW" H 3950 3950 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 3950 4050 60  0001 L CNN "Category"
F 7 "Transistors - FETs, MOSFETs - Arrays" H 3950 4150 60  0001 L CNN "Family"
F 8 "https://www.onsemi.com/pub/Collateral/2N7002DW-D.PDF" H 3950 4250 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/on-semiconductor/2N7002DW/2N7002DWCT-ND/1785790" H 3950 4350 60  0001 L CNN "DK_Detail_Page"
F 10 "MOSFET 2N-CH 60V 0.115A SC70-6" H 3950 4450 60  0001 L CNN "Description"
F 11 "ON Semiconductor" H 3950 4550 60  0001 L CNN "Manufacturer"
F 12 "Active" H 3950 4650 60  0001 L CNN "Status"
	1    3750 3450
	1    0    0    -1  
$EndComp
$Comp
L jbl~kikad~library:2N7002DW Q1
U 2 1 5EBC7142
P 3750 4450
F 0 "Q1" H 3750 4937 60  0000 C CNN
F 1 "2N7002DW" H 3750 4831 60  0000 C CNN
F 2 "digikey-footprints:SOT-363" H 3950 4650 60  0001 L CNN
F 3 "https://www.onsemi.com/pub/Collateral/2N7002DW-D.PDF" H 3950 4750 60  0001 L CNN
F 4 "2N7002DWCT-ND" H 3950 4850 60  0001 L CNN "Digi-Key_PN"
F 5 "2N7002DW" H 3950 4950 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 3950 5050 60  0001 L CNN "Category"
F 7 "Transistors - FETs, MOSFETs - Arrays" H 3950 5150 60  0001 L CNN "Family"
F 8 "https://www.onsemi.com/pub/Collateral/2N7002DW-D.PDF" H 3950 5250 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/on-semiconductor/2N7002DW/2N7002DWCT-ND/1785790" H 3950 5350 60  0001 L CNN "DK_Detail_Page"
F 10 "MOSFET 2N-CH 60V 0.115A SC70-6" H 3950 5450 60  0001 L CNN "Description"
F 11 "ON Semiconductor" H 3950 5550 60  0001 L CNN "Manufacturer"
F 12 "Active" H 3950 5650 60  0001 L CNN "Status"
	2    3750 4450
	1    0    0    -1  
$EndComp
$Comp
L dk_Transistors-FETs-MOSFETs-Single:2N7002 Q?
U 1 1 5EBB9F29
P 5850 3950
F 0 "Q?" H 5958 4003 60  0000 L CNN
F 1 "2N7002" H 5958 3897 60  0000 L CNN
F 2 "digikey-footprints:SOT-23-3" H 6050 4150 60  0001 L CNN
F 3 "https://www.onsemi.com/pub/Collateral/NDS7002A-D.PDF" H 6050 4250 60  0001 L CNN
F 4 "2N7002NCT-ND" H 6050 4350 60  0001 L CNN "Digi-Key_PN"
F 5 "2N7002" H 6050 4450 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 6050 4550 60  0001 L CNN "Category"
F 7 "Transistors - FETs, MOSFETs - Single" H 6050 4650 60  0001 L CNN "Family"
F 8 "https://www.onsemi.com/pub/Collateral/NDS7002A-D.PDF" H 6050 4750 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/on-semiconductor/2N7002/2N7002NCT-ND/244664" H 6050 4850 60  0001 L CNN "DK_Detail_Page"
F 10 "MOSFET N-CH 60V 115MA SOT-23" H 6050 4950 60  0001 L CNN "Description"
F 11 "ON Semiconductor" H 6050 5050 60  0001 L CNN "Manufacturer"
F 12 "Active" H 6050 5150 60  0001 L CNN "Status"
	1    5850 3950
	1    0    0    -1  
$EndComp
$EndSCHEMATC
